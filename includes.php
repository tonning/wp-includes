<?php 
/**
 * Include file by Kristoffer Tonning
 *
 * This is the main include file for additional libraries.
 * Its job is to include custom libraries and functions that are used regularly 
 *
 * @package Includes
 * @version 1.3.8
 */

/** --------------- -------------- --------------- **/
/** --------------- Enable Add-ons --------------- **/
/** --------------- -------------- --------------- **/

/** ---- ADMIN FRAMEWORKS ---- **/
add_fluent();
// add_titan();


/** ---- SLIDERS ---- **/
// add_action( 'wp_enqueue_scripts', 'add_lightSlider' );
// add_action( 'wp_enqueue_scripts', 'add_sliderPro' );


/** ---- ADDITIONAL LIBRARIES ---- **/
// add_action( 'wp_enqueue_scripts', 'add_animate' ); // Animate elements
// add_action( 'wp_enqueue_scripts', 'add_wow' ); // Animate elements as you scroll
// add_action( 'wp_enqueue_scripts', 'add_mixitup' ); // QuickSand sorting
// add_action( 'wp_enqueue_scripts', 'add_featherlight' ); // Lightbox
// add_mailchimp(); // MailChimp API support


/** ---- Enqueue CUSTOM FONTS ---- **/
// add_action('wp_print_styles', 'add_google_fonts');
// add_action('wp_print_styles', 'add_font_awesome'); // Font Awesome


/** ---- Remove Admin Bar ---- **/
add_filter('show_admin_bar', '__return_false');


/** ---- Remove All Dashboard Widgets ---- **/
// add_action('wp_dashboard_setup', 'remove_dashboard_widgets');


/** ---- Add Woocommerce Support ---- **/
// add_woocommerce_support();


/** ---- Add AJAX support ---- **/
add_action( 'wp_enqueue_scripts', 'register_ajax_javascript', 1 );
require_once( get_template_directory() . '/functions-ajax.php' );


/** ---- Add Custom Post Types ---- **/
add_custom_post_types();


/** ---- Hide Menu Pages ---- **/
// add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
	// remove_menu_page( 'index.php' );				// Dashboard
	// remove_menu_page( 'edit.php' );				// Posts
	// remove_menu_page( 'upload.php' );				// Media
	// remove_menu_page( 'edit.php?post_type=page' );	// Pages
	// remove_menu_page( 'edit-comments.php' );		// Comments
	// remove_menu_page( 'themes.php' );				// Appearance
	// remove_menu_page( 'plugins.php' );				// Plugins
	// remove_menu_page( 'users.php' );				// Users
	// remove_menu_page( 'tools.php' );				// Tools
	// remove_menu_page( 'options-general.php' );		// Settings
}




/** --------------- ---------------- --------------- **/
/** --------------- CUSTOM FUNCTIONS --------------- **/
/** --------------- ---------------- --------------- **/

/** --- Function to get image src from ID --- **/
function get_image_src($imageID, $size='full') {
	$imageSrc = $imageID; // For the default value
	if ( is_numeric( $imageID ) ) {
		$imageAttachment = wp_get_attachment_image_src( $imageID, $size );
		$imageSrc = $imageAttachment[0];
	}
	return $imageSrc;
}


/** --- List Mailchimp lists --- **/
function mailchimp_lists($api_key) {
	$MailChimp = new \Drewm\MailChimp($api_key);
	return $MailChimp->call('lists/list');
}


/** --- Subscribe to Mailchimp list --- **/
function mailchimp_subscribe($api_key, $list_id, $email, $first_name = NULL, $last_name = NULL) {
	$MailChimp = new \Drewm\MailChimp($api_key);
	$mailResult = $MailChimp->call('lists/subscribe', array(
		'id'				=> $list_id,
		'email'             => array('email'=>$email),
		'merge_vars'        => array('FNAME'=>$first_name, 'LNAME'=>$last_name),
		'double_optin'      => false,
		'update_existing'   => false,
		'replace_interests' => false,
		'send_welcome'      => false,
	));
	return $mailResult;
}

function my_get_post_type() {
	global $post_type, $post_id;
	if (isset($_GET['post_type'])) {
		$post_type = $_GET['post_type'];
	} else {
		$post_id = (isset($_GET['post'])) ? $_GET['post'] : $_POST['post_ID'];
		$post_type = get_post_type($post_id);
	}

	return $post_type;
}

function valid_pass($candidate) {
    if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $candidate))
        return FALSE;
    return TRUE;

    /*
	    Explaining $\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$
	    $ = beginning of string
	    \S* = any set of characters
	    (?=\S{8,}) = of at least length 8
	    (?=\S*[a-z]) = containing at least one lowercase letter
	    (?=\S*[A-Z]) = and at least one uppercase letter
	    (?=\S*[\d]) = and at least one number
	    (?=\S*[\W]) = and at least a special character (non-word characters)
	    $ = end of the string
	 */
}





/** ---- ------------------------------------------------------- ---- **/
/** ---- DON'T EDIT BELOW HERE UNLESS YOU KNOW WHAT YOU'RE DOING ---- **/
/** ---- ------------------------------------------------------- ---- **/

/** ---- STANDARD ADD-ONS ---- **/
add_action( 'wp_enqueue_scripts', 'add_bootstrap' ); // Add bootstrap
add_action( 'wp_enqueue_scripts', 'add_custom_stylesheet' ); // Add /css/theme-style.css
add_theme_support( 'post-thumbnails' ); // Add support for thumbnails on custom post types


/** Register function.js and localize admin-ajax.php */
function register_ajax_javascript() {

	/** Register JavaScript Functions File */
	wp_register_script( 'functions-js', esc_url( trailingslashit( get_template_directory_uri() ) . 'functions.js' ), array( 'jquery' ), '1.0', true );

	/** Localize Scripts */
	$php_array = array( 'admin_ajax' => admin_url( 'admin-ajax.php'), 'template_directory' => get_template_directory_uri() );
	wp_localize_script( 'functions-js', 'php_array', $php_array );

	/** Enqueue JavaScript Functions File */
	wp_enqueue_script( 'functions-js' );

}

/** --- Embed FLUENT Framework --- **/
function add_fluent() {
	include_once(get_template_directory().'/lib/fluent-framework/fluent-framework.php');
	Fluent_Base::$url = get_template_directory_uri().'/lib/fluent-framework/';
	require_once ( get_template_directory() . '/admin/admin-init.php' );
}

/** --- Embed TITAN Framework --- **/
function add_titan() {
	require_once ( get_template_directory() . '/lib/titan-framework/titan-framework-embedder.php' );
	require_once ( get_template_directory() . '/admin/titan/theme-options.php' );
	require_once ( get_template_directory() . '/admin/titan/meta-options.php' );
}

function add_custom_post_types() {
	require_once ( get_template_directory() . '/custom-post-types.php' );	
}

/** --- Enqueue MAILCHIMP API --- **/
function add_mailchimp() {
	require_once ( get_template_directory() . '/lib/mailchimp/MailChimp.php' );
}

/** --- Enqueue BOOTSTRAP --- **/
function add_bootstrap() {
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/lib/bootstrap/js/bootstrap.min.js', array( 'jquery' ), '3.0.1', true );
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/lib/bootstrap/css/bootstrap.min.css', array(), '3.3.0', 'all' );
	// Register Custom Navigation Walker
	require_once('lib/wp-bootstrap-navwalker/wp_bootstrap_navwalker.php');
}

/** --- Enqueue LIGHTSLIDER --- **/
function add_lightSlider() {
	wp_enqueue_script( 'lightSlider-js', get_template_directory_uri() . '/lib/lightSlider/js/jquery.lightSlider.min.js', array( 'jquery' ), '3.0.1', true );
	wp_enqueue_style( 'lightSlider-css', get_template_directory_uri() . '/lib/lightSlider/css/lightSlider.css', array(), '1.1.0', 'all' );
}

/** --- Enqueue ANIMATE.css --- **/
function add_animate() {
	wp_enqueue_style( 'animate-style', get_template_directory_uri() . '/lib/animate/animate.css' );
}

/** --- Enqueue WOW --- **/
function add_wow() {
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/lib/wow/wow.min.js' );
}

/** --- Enqueue MIXITUP --- **/
function add_mixitup() {
	wp_enqueue_script( 'mixitup-js', get_template_directory_uri() . '/lib/mixitup/jquery.mixitup.js', array( 'jquery' ), '3.0.1', true );
}

/** --- Enqueue SLIDERPRO --- **/
function add_sliderPro() {
	wp_enqueue_script( 'sliderPro-js', get_template_directory_uri() . '/lib/sliderPro/jquery.sliderPro.min.js', array( 'jquery' ), '3.0.1', true );
	wp_enqueue_style( 'sliderPro-css', get_template_directory_uri() . '/lib/sliderPro/slider-pro.min.css', array(), '1.0.8', 'all' );
}

/** --- Enqueue FEATHERLIGHT --- **/
function add_featherlight() {
	wp_enqueue_script( 'featherlight-js', get_template_directory_uri() . '/lib/featherlight/featherlight.min.js', array( 'jquery' ), '3.0.1', true );
	wp_enqueue_style( 'featherlight-css', get_template_directory_uri() . '/lib/featherlight/featherlight.min.css', array(), '1.0.8', 'all' );
}

/** --- Add GOOGLE FONTS --- **/
function add_google_fonts() {
	wp_register_style('GoogleFonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,700');
	wp_enqueue_style('GoogleFonts');
}

/** --- Add FONT AWESOME --- **/
function add_font_awesome() {
	wp_register_style('Font Awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
	wp_enqueue_style('Font Awesome');
}

/** --- Enqueue THEME-STYLE.css --- **/
function add_custom_stylesheet() {
	wp_enqueue_style( 'custom-style-screen', get_template_directory_uri() . '/css/theme-style.css' );
}

/** --- Remove Standard Wordpress Dashboard Widget --- **/
function remove_dashboard_widgets(){
	// use 'dashboard-network' as the second parameter to remove widgets from a network dashboard.
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');   // Activity
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
	remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
	remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
	remove_action( 'welcome_panel', 'wp_welcome_panel' ); // Welcome Panel
}

/** --- Add WooCommerce Support and remove nag --- **/
function add_woocommerce_support() {
	// Add WooCommerce Support
	add_theme_support( 'woocommerce' );

	// Removes WooThemes Updater Nag
	remove_action( 'admin_notices', 'woothemes_updater_notice' );	
}

/** --- Fluent Framework related variables and functions --- **/
// If Fluent Framework is activated
if (defined('FLUENT_FILE')) {
	// Set global theme options variable
	$options = get_option('theme-options');

	// Add favicon if set in theme-options
	if ( (isset( $options['favicon'] )) && $options['favicon'] ) {
		function blog_favicon() {
			global $options;
			echo '<link rel="shortcut icon" href="' . get_image_src($options['favicon']) . '">';
		}
		add_action('wp_head', 'blog_favicon');
	}
}
