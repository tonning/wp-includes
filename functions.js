/**
 * AJAX functions
 *
 * @package wp-includes
 * @since 1.2.0
 */
(function($) {
		  
	function SOME_FUNCTION() {
	   $.ajax({
			cache: false,
			timeout: 8000,
			url: php_array.admin_ajax,
			type: "POST",
			dataType: "json",
			data: ({ 
				action:'JS_FUNCTION_NAME',
			}),

			beforeSend: function() {
			},

			success: function(data) {

			},

			error: function( jqXHR, textStatus, errorThrown ){
				console.log( 'The following error occured: ' + jqXHR, textStatus, errorThrown );   
			},
	   });
	   return false;
	}
 
})(jQuery);