<?php
/**
 * Fluent Admin Metabox Init
 *
 * @package wp-includes
 * @since 1.2.0
 */
global $post_id;
require_once( dirname(__FILE__) . '/theme-options.php' );

$filename = basename($_SERVER["SCRIPT_FILENAME"]);

if ( isset($_GET['post']) OR (isset($_POST['action']) AND ($_POST['action'] == 'editpost')) OR (($filename == 'post-new.php') AND ($_GET['post_type'])) ) {

	$post_type = my_get_post_type();

	if ($post_type == 'page') {
		$page_template = (get_post_meta($post_id, '_wp_page_template', true)) ? get_post_meta($post_id, '_wp_page_template', true) : 'page-default';
		if (($page_template == 'default') OR ($page_template == 'page-default')) { 
			$post_type = 'page-default.php';
		} else {
			$post_type = $page_template;
		}
	} else {
		$post_type .= '.php';
	}

	if ( file_exists( dirname(__FILE__) . '/meta/' . $post_type ) ) {
		require_once( dirname(__FILE__) . '/meta/' . $post_type );
	}
	
}