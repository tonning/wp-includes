<?php
/**
 * Fluent Admin Metabox page
 *
 * @package wp-includes
 * @since 1.2.0
 */

$meta = array();

$meta['[NAME]'] = array(
	'title'     => __('Info bokse', '[TEXT DOMAIN]'),
	'context'   => 'normal',
	'priority'  => 'high',
	'fields'    =>  array(
		'field_name' => array(
			'title' => __('Title', '[TEXT DOMAIN]'),
			'type' => 'text',
		),
	),
);

//load meta boxes
$args = array(
	'dev_mode' => false,
	'option_name' => '[OPTION NAME]',
	'post_types' => array(my_get_post_type()),
);
Fluent_Store::set('metaboxes', new Fluent_Options_Meta( $args, $meta ));

/** --- Removes metaboxes from post/page admin --- **/
function remove_metaboxes() {
	$post_type = my_get_post_type();
	$remove_meta_boxes = array(
		// 'title',
		// 'editor',
		// 'author', // Author metabox same as 'authordiv'
		// 'thumbnail', // same as 'postimagediv'
		// 'excerpt', // Excerpt metabox same as 'postexcerpt'
		// 'trackbacks', // Trackbacks metabox same as 'trackbacksdiv'
		// 'custom-fields', // same as 'postcustom'
		// 'comments', // Comments metabox (also will see comment count balloon on edit screen) same as 'commentsdiv'
		// 'revisions', // (will store revisions) same as 'revisionsdiv'
		// 'page-attributes', // (template and menu order) (hierarchical must be true) same as 'pageparentdiv'
		// 'post-formats', // removes post formats, see Post Formats
		// 'categorydiv', // Categories metabox.
		// 'commentstatusdiv', // Comments status metabox (discussion)
		// 'formatdiv', // Formats metabox
		// 'slugdiv', // Slug metabox
		// 'submitdiv', // Date, status, and update/save metabox
		// 'tagsdiv-post_tag', // Tags metabox
		// '{$tax-name}div', // Hierarchical custom taxonomies metabox
	);

	foreach ( $remove_meta_boxes as $field => $value ) {
		remove_post_type_support( $post_type, $value );
		remove_meta_box( $value, $post_type, 'normal');
	}
}
add_action( 'admin_init', 'remove_metaboxes' );
