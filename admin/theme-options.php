<?php
/**
 * Fluent Admin Option Page
 *
 * @package wp-includes
 * @since 1.2.0
 */

//sections are groups of fields, displayed as metaboxes or blocks depending on the usage case
$sections = array();

//each section has attributes like title and icon to be used, and has fields attached to it
//basic text fields
$sections['general'] = array(
	'dash_icon' => 'list-view',
	'title' =>  __('Generelt', '[TEXT DOMAIN]'),
	'context' => 'normal',
	'priority' => 'high',
	'caps' => array(),
	'fields' => array(
		'logo' => array(
			'type' => 'media',
			'title' => __('Logo', '[TEXT DOMAIN]'),
		),
		'favicon' => array(
			'type' => 'media',
			'title' => __('Favicon', '[TEXT DOMAIN]'),
		),
	),
);

$sections['footer'] = array(
	'dash_icon' => 'list-view',
	'title' =>  __('Footer', '[TEXT DOMAIN]'),
	'context' => 'normal',
	'priority' => 'high',
	'caps' => array(),
	'fields' => array(
		'copyright' => array(
			'type' => 'text',
			'title' => __('Copyright', 'cratebuddy'),
			'description' => __('<strong>Available shortcodes</strong><br>Present year: [year]<br>Copyright symbol: [c]'),
		),
	),
);

//load normal options page
$args = array(
	'dev_mode' => false,
	'option_name' => 'theme-options',
	'page_args' => array(
		'slug' => 'theme-options',//the unique slug of the page
		'menu_title' => __( 'Theme Options', '[TEXT DOMAIN]' ),//the title in the sidebar menu of the page
		'page_title' => __( 'Theme Options', '[TEXT DOMAIN]' ),//the page title when rendered
		'parent' => '',//a string referencing the parent menu item if any, e.g: 'admin.php?page=somepage'
		'cap' => 'manage_options',//the capability of users who can access this page
		'priority' => null,//the menu item priority
		'menu_icon' => '',//the dash icon or url to image icon for use in the menu (only for top level pages)
		'page_icon' => 'icon-themes',//the dash icon if any to use on the page when rendered
		'callback' => false//the page render callback
	),
	'restore' => false,//false to disable the restore option
	'show_updated' => true,//false to disable the last updated time
	
	// Danish translation
	// 'messages' => array(//here you can provide custom messages which will overwrite the default ones used on different actions
	//     'save_button' => __('Gem Indstillinger', '[TEXT DOMAIN]'),//displays in the settings save box
	//     'saved' => __('Indstillinger Gemt', '[TEXT DOMAIN]'),//displays in the settings saved notice
	// )
);
$panel = new Fluent_Options_Page( $args, $sections );